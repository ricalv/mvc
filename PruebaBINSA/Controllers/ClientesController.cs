﻿using PruebaBINSA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaBINSA.Controllers
{
    public class ClientesController : Controller
    {
        private ApplicationDbContext _context;

        public ClientesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Clientes
        public ActionResult Index()
        {
            var clientes = _context.Clientes.ToList();

            return View(clientes);
        }

        public ActionResult FormClientes()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Save(Cliente cliente)
        {
            if (!ModelState.IsValid)
            {
                return View("FormClientes", cliente);
            }

            if (cliente.Id == 0)
                _context.Clientes.Add(cliente);
            else
            {
                var clienteInDB = _context.Clientes.Single(c => c.Id == cliente.Id);

                clienteInDB.Nombre = cliente.Nombre;
                clienteInDB.Domicilio = cliente.Domicilio;
                clienteInDB.CodigoPostal = cliente.CodigoPostal;
                clienteInDB.Poblacion = cliente.Poblacion; 
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Clientes");
                
         }

        public ActionResult Edit(int Id)
        {
            var cliente = _context.Clientes.SingleOrDefault(c => c.Id == Id);

            return View("FormClientes",cliente);
        }

        [HttpDelete]
        public void Delete(int Id)
        {
            var contactos = _context.ContactosClientes.SingleOrDefault(c => c.ClienteId == Id);
            var cliente = _context.Clientes.SingleOrDefault(c => c.Id == Id);

            _context.ContactosClientes.Remove(contactos);
            _context.Clientes.Remove(cliente);
            _context.SaveChanges();

        }
    }
}
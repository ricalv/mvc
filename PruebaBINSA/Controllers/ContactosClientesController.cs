﻿using PruebaBINSA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using PruebaBINSA.ViewModels;


namespace PruebaBINSA.Controllers
{
    public class ContactosClientesController : Controller
    {
        private ApplicationDbContext _context;

        public ContactosClientesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: ContactosClientes
        public ActionResult Index()
        {
             var contactos = _context.ContactosClientes.Include(c => c.Cliente).ToList();

            return View(contactos);
        }

        public ActionResult Edit(int Id)
        {
            var contacto = _context.ContactosClientes.SingleOrDefault(c => c.Id == Id);

            var viewModel = new FormContactoViewModel
            {
                ContactosClientes = contacto,
                Clientes = _context.Clientes.ToList()
            };

            return View("FormContactos", viewModel);

        }

        [HttpPost]
        public ActionResult Save(ContactosCliente contacto)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new FormContactoViewModel
                {
                    ContactosClientes = contacto,
                    Clientes = _context.Clientes.ToList()
                };

               return View("FormContactos", viewModel);
            }

            if (contacto.Id == 0)
            {
                _context.ContactosClientes.Add(contacto);
            }
            else
            {
                var contactoInDb = _context.ContactosClientes.Single(c => c.Id == contacto.Id);
                contactoInDb.ClienteId = contacto.ClienteId;
                contactoInDb.Nombre = contacto.Nombre;
                contactoInDb.Telefono = contacto.Telefono;
                contactoInDb.Email = contacto.Email;

            };

            _context.SaveChanges();

            return RedirectToAction("Index", "ContactosClientes");

        }

        public ActionResult FormContactos()
        {
            var clientes = _context.Clientes.ToList();

            var viewModel = new FormContactoViewModel {
                Clientes = clientes,
                ContactosClientes = new ContactosCliente()
            };
            return View(viewModel);
        }

        public ActionResult Delete(int Id)
        {
            var contacto = _context.ContactosClientes.SingleOrDefault(c => c.Id == Id);

            _context.ContactosClientes.Remove(contacto);
            _context.SaveChanges();

            return RedirectToAction("Index","ContactosClientes");
        }

    }
}
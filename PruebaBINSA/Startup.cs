﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PruebaBINSA.Startup))]
namespace PruebaBINSA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

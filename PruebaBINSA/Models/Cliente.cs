﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaBINSA.Models
{
    public class Cliente
    {
        public int Id { get; set; }

        [StringLength(40)]
        [Required]
        public string Nombre { get; set; }

        [StringLength(40)]
        public string Domicilio { get; set; }

        [StringLength(5)]
        public string CodigoPostal { get; set; }

        [StringLength(40)]
        public string Poblacion { get; set; }
    }
}
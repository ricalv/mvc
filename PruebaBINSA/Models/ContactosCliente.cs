﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaBINSA.Models
{
    public class ContactosCliente
    {
        public int Id { get; set; }

        public Cliente Cliente { get; set; }

        public int ClienteId { get; set; }

        [StringLength(40)]
        public string Nombre { get; set; }

        [StringLength(40)]
        public string Telefono { get; set; }

        [StringLength(40)]
        public string Email { get; set; }
    }
}
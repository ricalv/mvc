﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PruebaBINSA.Models;

namespace PruebaBINSA.ViewModels
{
    public class FormContactoViewModel
    {
        public IEnumerable<Cliente> Clientes { get; set; }
        public ContactosCliente ContactosClientes { get; set; }
    }
}
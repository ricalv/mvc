namespace PruebaBINSA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddContactoClienteFix : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactosClientes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        Nombre = c.String(maxLength: 40),
                        Telefono = c.String(maxLength: 40),
                        Email = c.String(maxLength: 40),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .Index(t => t.ClienteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContactosClientes", "ClienteId", "dbo.Clientes");
            DropIndex("dbo.ContactosClientes", new[] { "ClienteId" });
            DropTable("dbo.ContactosClientes");
        }
    }
}

namespace PruebaBINSA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateContactosClientes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (1,'Contacto 1','88990298','contacto1@mail.com')");
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (1,'Contacto 2','99839093','contacto2@mail.com')");
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (2,'Contacto 1','74837892','contacto1@mail.com')");
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (2,'Contacto 2','03827844','contacto2@mail.com')");
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (3,'Contacto 1','11940393','contacto1@mail.com')");
            Sql("INSERT INTO ContactosClientes (ClienteId, Nombre, Telefono, Email) VALUES (3,'Contacto 2','42993045','contacto2@mail.com')");
        }
        
        public override void Down()
        {
        }
    }
}

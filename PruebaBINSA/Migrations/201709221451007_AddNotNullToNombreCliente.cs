namespace PruebaBINSA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNotNullToNombreCliente : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clientes", "Nombre", c => c.String(nullable: false, maxLength: 40));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clientes", "Nombre", c => c.String(maxLength: 40));
        }
    }
}

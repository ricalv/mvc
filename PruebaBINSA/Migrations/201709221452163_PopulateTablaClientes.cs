namespace PruebaBINSA.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTablaClientes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Clientes (Nombre, Domicilio, CodigoPostal, Poblacion) VALUES ('Ricardo Miguel','AV PERU 34','010','500')");
            Sql("INSERT INTO Clientes (Nombre, Domicilio, CodigoPostal, Poblacion) VALUES ('Raul Ernesto','AV LIMA 40','020','400')");
            Sql("INSERT INTO Clientes (Nombre, Domicilio, CodigoPostal, Poblacion) VALUES ('Rodrigo Alfonso','AV CALLAO 10','060','900')");
        }
        
        public override void Down()
        {
        }
    }
}
